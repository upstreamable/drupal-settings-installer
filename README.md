# Drupal settings installer

This package takes over the settings.php file for Drupal using scaffolding from `drupal/core-composer-scaffold`
and allows it to manage Drupal settings from composer packages with the type `drupal-settings` in the `vendor` directory.

## Install

```
composer require upstreamable/drupal-settings-installer
```

The package automatically adds itself to the allowed packages list for `drupal/core-composer-scaffold`.
After that [upstreamable/drupal-settings-db-env](https://packagist.org/packages/upstreamable/drupal-settings-db-env)
can be required to manage the database credentials using environment variables
or [upstreamable/drupal-settings-db-sqlite-private](https://packagist.org/packages/upstreamable/drupal-settings-sqlite-private)
if just sqlite is needed.

## Why?

Composer project templates are great, except they are a "fork and forget" model.
This means various project files can drift and lose updates when there are upstream improvements.
The initial `settings.php` file installed with a project template or a site install is a monolyth
that doesn't support environment variables and mixes site settings, database credentials and configuration overrides.

This package aims to solve that problem by allowing small chunks of settings to be included by requiring a composer package.

For example if you have some development-only settings you can add those settings to the `require-dev` section of your root composer.json.

## How does it work?

The `settings.php` included searches for composer packages with the type `drupal-settings` among the composer packages installed.
Each package has a weight determined by the patch number in its version. For example a package with a `1.0.20` version will a have weight of `20`.
Heavier settings will be aplied later and therefore overwrite defaults set by this plugin or lighter settings.
See [upstreamable/drupal-settings-db-env](https://gitlab.com/upstreamable/drupal-settings-db-env)
as an example.

This also implies that the patch version number will not have the usual meaning in the package
and only the major and minor numbers will be increased.

## Defaults

The plugin sets some defaults that can be seen in the `assets/default.settings.php` file.
Those can be overridden by having a settings file that modifies their value or, in some cases,
with an environment variable like DRUPAL_HASH_SALT.

## Custom settings

Although some code can be appended to the scaffolded `setting.php` it is better to create a
local composer package so all the settings are in the vendor folder..
A `packages` directory needs to be created and added as a repository to the root `composer.json`, for example:
```
    "repositories": [
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
            "type": "path",
            "url": "packages/*"
        }
    ],
```

Then create a `packages/custom-settings` with a minimal `composer.json` file like this:

```

{
    "name": "upstreamable/custom-settings",
    "version": "1.0.99",
    "type": "drupal-settings",
}
```
Note the `99` in the patch version number to get a high weight that will override settings from lighter packages.
At the same level create a `settings.php` with custom settings, for example:
```
<?php
$settings['config_sync_directory'] = '../config/sync';
```
This overrides the default settings of using `../config` as a sync directory.

Finally run `composer require upstreamable/custom-settings` and the new value should take effect.

## More packages with drupal settings

* [Redis](https://packagist.org/packages/upstreamable/drupal-settings-redis-env)
* [Symfony mailer and SMTP](https://packagist.org/packages/upstreamable/drupal-settings-symfony-mailer-smtp-env)

[Packages published in packagist.org](https://packagist.org/?query=&type=drupal-settings)

### Development packages

* [Development configurations](https://packagist.org/packages/upstreamable/drupal-settings-devel-env)

## Credits

It was inspired by [`bluehorndigital/mojo-drupal-scaffold`](https://github.com/bluehorndigital/mojo-drupal-scaffold) which provides files to setup a Drupal project.
The Drupal settings installer only takes care of the `settings.php` system and doesn't decide how environment variables
should be provided (as system variables in a docker container or through a `.env` file to name two examples).
The mojo drupal scaffold project also provides configuration for contrib modules such as redis or flysystem but with this plugin those
can be separated into their own projects.
