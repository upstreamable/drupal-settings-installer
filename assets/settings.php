<?php

// phpcs:ignoreFile

use Composer\InstalledVersions;

$packages = [];
// From InstalledVersions::getInstalledPackagesByType() but adapted to do all the processing in one go.
foreach (InstalledVersions::getAllRawData() as $installed) {
  foreach ($installed['versions'] as $name => $package) {
    if (isset($package['type']) && $package['type'] === 'drupal-settings') {
      $matches = [];
      preg_match("/^\d+\.\d+\.(\d+).+$/i", $package['version'], $matches);
      $packages[$package['install_path'] . '/settings.php'] = intval($matches[1]);
    }
    if ($name === 'upstreamable/drupal-settings-installer') {
      $packages[$package['install_path'] . '/assets/default.settings.php'] = 0;
    }
  }
}
asort($packages);
foreach (array_keys($packages) as $packagePath) {
  include $packagePath;
}
