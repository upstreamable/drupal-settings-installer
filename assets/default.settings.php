<?php

// phpcs:ignoreFile

$settings['config_sync_directory'] = '../config';

/**
 * Salt for one-time login links, cancel links, form tokens, etc.
 */
$settings['hash_salt'] = getenv('DRUPAL_HASH_SALT') ?: '';

$settings['file_public_path'] = getenv('DRUPAL_PUBLIC_PATH') ?: 'sites/default/files';

$settings['state_cache'] = !(getenv('DRUPAL_STATE_CACHE') === 'false');

/**
 * Private file path:
 *
 * A local file system path where private files will be stored. This directory
 * must be absolute, outside of the Drupal installation directory and not
 * accessible over the web.
 *
 * Use the default path from the wodby drupal docker images.
 */
if (getenv('DRUPAL_PRIVATE_PATH')) {
  $settings['file_private_path'] = getenv('DRUPAL_PRIVATE_PATH');
}

/**
 * Temporary file path:
 *
 * A local file system path where temporary files will be stored. This directory
 * must be absolute, outside the Drupal installation directory and not
 * accessible over the web.
 */
$settings['file_temp_path'] = sys_get_temp_dir();

/**
 * Deployment identifier.
 *
 * Drupal's dependency injection container will be automatically invalidated and
 * rebuilt when the Drupal core version changes. When updating contributed or
 * custom code that changes the container, changing this identifier will also
 * allow the container to be invalidated as soon as code is deployed.
 */
$settings['deployment_identifier'] = getenv('DEPLOYMENT_IDENTIFIER') ?: \Drupal::VERSION;

/**
 * Security hardening.
 */
$settings['update_free_access'] = FALSE;
$settings['allow_authorize_operations'] = FALSE;

/**
 * Default mode for directories and files written by Drupal.
 *
 * Ensures group level access.
 */
$settings['file_chmod_directory'] = 0775;
$settings['file_chmod_file'] = 0664;

/**
 * The default list of directories that will be ignored by Drupal's file API.
 *
 * By default, ignore node_modules and bower_components folders to avoid issues
 * with common frontend tools and recursive scanning of directories looking for
 * extensions.
 *
 * @see \Drupal\Core\File\FileSystemInterface::scanDirectory()
 * @see \Drupal\Core\Extension\ExtensionDiscovery::scanDirectory()
 */
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
  'vendor',
];

/**
 * Trusted host configuration.
 *
 * Drupal core can use the Symfony trusted host mechanism to prevent HTTP Host
 * header spoofing.
 *
 * Generally not required if the server only allows verified domains to be
 * routed to the server.
 *
 * @see default.settings.php for more information.
 */
$settings['trusted_host_patterns'][] = '.*';
