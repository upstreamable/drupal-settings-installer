# Changelog

## 1.0.6
* Set `overwrite` to false to play better with `ddev` since this plugin was ovewriting the settings.php on every composer install.
* Do not set a default for drupal private path.
